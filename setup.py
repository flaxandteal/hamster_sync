from setuptools import setup, find_packages

setup(
    name='hamster_sync',
    version='0.1',
    description='Syncing Hamster to cloud time-tracking platforms',
    url='https://gitlab.com/flaxandteal/hamster_sync',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    install_requires=[
        'Click',
        'pydbus',
        'togglwrapper'
    ],
    entry_points='''
        [console_scripts]
        hamster_sync=hamster_sync.scripts.hamster_sync:cli
    '''
)
