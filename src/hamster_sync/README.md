# Hamster Sync

Config file should be in, e.g. ~/.config/hamster_sync/config.yaml and contain:

```yaml
toggl:
  api_key: [APIKEY]
  wid: [WORKSPACEID]
```
