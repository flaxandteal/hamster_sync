import yaml
import os

from appdirs import user_config_dir

class Config:
    def __init__(self):
        self._conf = None
        self._filename = 'config.yaml'

    def setup(self):
        self._conf = {
            'toggl': {
                'api_key': None
            }
        }
        config_dir = user_config_dir('hamster_sync', 'hamster_sync')

        os.makedirs(config_dir, exist_ok=True)

        try:
            with open(os.path.join(config_dir, self._filename), 'r') as f:
                self._conf.update(yaml.load(f))
        except IOError:
            pass

    def get(self):
        return self._conf
