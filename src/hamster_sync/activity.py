class HamsterDriverActivity:
    def __init__(self, driver, upstream=None):
        self._driver = driver
        self._upstream = upstream

    def update(self, activity):
        print("Updating %s" % self._driver.get_id())
        self._upstream = self._driver.sync(activity, old=self._upstream)

    def pull(self):
        print("Pulling %s" % self._driver.get_id())
        pair = self._driver.get()
        print('pulled', pair)

        if pair:
            self._upstream, activity_dict = pair
            return activity_dict

        return None

class HamsterActivity:
    """Summarize a single piece of Hamster activity"""

    fields = ('fact_id', 'start_time', 'end_time', 'x1', 'description', 'id', 'category', 'x4', 'x5', 'x6')

    _drivers = None
    _activity = None

    @classmethod
    def pull(cls, drivers):
        """Create an Activity based on a running driver activity if one exists."""

        activity = None
        activity_dict = None
        driver_activities = {}

        for driver in drivers:
            driver_activity = HamsterDriverActivity(driver)
            driver_activities[driver.get_id()] = driver_activity

            activity_dict = activity_dict or driver_activity.pull()

        if activity_dict:
            activity = cls(activity_dict, drivers)
            activity.sync()

        return activity

    def __init__(self, activity_dict=None, drivers=None):
        if drivers is not None:
            self._drivers = {driver.get_id(): HamsterDriverActivity(driver) for driver in drivers}

        if activity_dict is not None:
            self._activity = activity_dict
            for field in self.fields:
                setattr(self, field, self._activity[field])

    def set(self, driver, upstream):
        """Attach an upstream activity to this Hamster activity"""

        self._drivers[driver.get_id()] = HamsterDriverActivity(
            driver,
            self._activity,
            upstream
        )

    def __eq__(self, other):
        """Check whether two pieces of activity are talking about the same thing.

        Note, not necessarily same end time.
        """

        if self.id and other.id:
            return self.id == other.id

        print('eq', self.start_time, self.description, other.start_time, other.description)
        return self.start_time == other.start_time and self.description == other.description

    def get(self, driver):
        """Provide any prior upstream entry for this activity"""
        driver_id = driver.get_id()

        if driver_id in self._drivers:
            return self._drivers[driver_id]

        return None

    def copy(self):
        """Give a new version of this object."""

        new = HamsterActivity()
        new._activity = self._activity
        new._drivers = self._drivers

    def sync(self):
        """Make sure all drivers match the current activity."""

        self.update(self, sync_first=False)

    def update(self, other, sync_first=True):
        """Update this activity, and make sure all drivers match."""

        if other != self:
            # This is a different activity
            return False

        if sync_first:
            self.sync()

        for driver in self._drivers.values():
            driver.update(other)

        self._activity = other._activity
        for field in self.fields:
            setattr(self, field, self._activity[field])

    def isActive(self):
        """Check whether this activity is in-process."""
        return not bool(self.end_time)

    def create(self):
        """Ensure activity is running everywhere."""

        for driver_activity in self._drivers.values():
            driver_activity.create(self)

    def stop(self):
        """Ensure activity is stopped everywhere."""

        for driver_activity in self._drivers.values():
            driver_activity.stop(self)
