from .activity import HamsterActivity

class HamsterSync:
    """Management class, for consolidating Hamster events and handling
    activities.
    """
    _last = None

    def __init__(self, driver, hamster_factory, config):
        self._config = config.get()
        self._drivers = [driver.make(config=config)]
        self._hamster = hamster_factory.make(
            onChanged=self.onChanged,
            onToggled=self.onToggled
        )

    def pull(self):
        self._last = HamsterActivity.pull(self._drivers)
        if self._last:
            fact = self._hamster.AddFact(
                self._last.description,
                int(self._last.start_time),
                int(self._last.end_time) if self._last.end_time else 0,
                False
            )

    def onToggled(self):
        """Identify toggling."""
        print('toggle')

    def onChanged(self):
        """When facts change locally..."""
        facts = self._hamster.GetTodaysFacts()
        if facts:
            activity_dict = dict(zip(HamsterActivity.fields, facts[-1]))
            fact = HamsterActivity(activity_dict, self._drivers)
            if not self._last or self._last != fact:
                self._last = fact
                self._last.sync()
            else:
                self._last.update(fact)
            # elif self._last == fact and self._last.isActive():
            #     self._last.stop()
