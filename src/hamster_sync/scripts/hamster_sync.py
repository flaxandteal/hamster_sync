from gi.repository import GLib
import click
from hamster_sync.toggl_driver import TogglDriver
from hamster_sync.bus import HamsterBus
from hamster_sync.sync import HamsterSync
from hamster_sync.config import Config

@click.command()
def cli():
    config = Config()
    config.setup()

    bus = HamsterBus(config)
    hamster_toggl = HamsterSync(
        TogglDriver,
        bus,
        config
    )
    hamster_toggl.pull()

    click.echo('Starting sync observation...')
    GLib.MainLoop().run()
