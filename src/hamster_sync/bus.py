from pydbus import SessionBus

class HamsterBus:
    """Factory for DBUS bus, linking to Hamster applet."""
    def __init__(self, config):
        self._bus = SessionBus()
        self._config = config

    def make(self, onChanged=None, onToggled=None):
        """Create a new DBUS object to interact with."""

        bus = self._bus.get("org.gnome.Hamster", "/org/gnome/Hamster")

        bus.onFactsChanged = onChanged
        bus.onToggleCalled = onToggled

        return bus
