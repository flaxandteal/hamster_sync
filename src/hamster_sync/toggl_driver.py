from togglwrapper import Toggl
import time
import dateutil.parser
from datetime import datetime, timedelta

# HamsterSync fields
#  ('fact_id', 'start_time', 'end_time', 'x1', 'description', 'id', 'category', 'x4', 'x5', 'x6')
# TIMEENTRY
#
#     {'data':
#       {
#           'duration': -1509712200,
#           'uid': 3498954,
#           'duronly': False,
#           'start': '2017-11-03T10:30:00Z',
#           'at': '2017-11-03T12:30:25+00:00',
#           'description': 'kops work',
#           'billable': False,
#           'id': 722902349,
#           'wid': 2348596
#       }
#     }

class TogglDriver:
    """Hook into Toggl activity."""
    _toggl = None
    _config = None
    _throttle_secs = 1
    _throttle_limit = 10
    _throttle_last = None
    _throttle_count = 0

    def __init__(self, config):
        self._config = config.get()['toggl']

        api_key = self._config['api_key']
        if not api_key:
            raise RuntimeError('No API key for Toggl in configuration')

        self._toggl = Toggl(self._config['api_key'])
        self._id = 'toggl'

    def get_id(self):
        """Identify this specific driver."""
        return self._id

    @classmethod
    def make(cls, config):
        """Create a new instance of the driver."""
        return cls(config)

    def sync(self, activity, old=None):
        """Run necessary updates to ensure old upstream matches activity."""

        print(activity._activity, old)
        if not activity.end_time:
            if not old or old['data']['duration'] > 0:
                return self.create(activity)
        else:
            if old and old['data']['duration'] <= 0:
                return self.stop(old)

        return old

    def create(self, activity):
        """Create an activity."""

        print('Creating')
        start = datetime.fromtimestamp(int(activity.start_time))
        desc = activity.description
        if activity.category:
            desc += '@' + activity.category

        request = {
            'description': desc,
            'wid': self._config['wid'],
            'created_with': 'Hamster Sync',
            'start': start.isoformat() + 'Z',
            'duration': -activity.start_time
        }
        result = self._call(self._toggl.TimeEntries.create, {'time_entry': request})

        return result

    def stop(self, old):
        """Stop an activity."""

        result = self._call(self._toggl.TimeEntries.stop, old['data']['id'])

        return result

    def get(self):
        """Get current time entry, if one exists."""

        earliest_start = datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)
        result = self._call(self._toggl.TimeEntries.get, start_date=earliest_start.isoformat() + 'Z')

        active = [r for r in result if r['duration'] < 0]

        if active:
            # We do not have means of syncing multiple active time entries
            upstream = active[-1]

            start_time = dateutil.parser.parse(upstream['start'])
            description = upstream['description']
            category = None
            if '@' in description:
                description, category = description.split('@')

            return upstream, {
                'fact_id': None,
                'start_time': int(time.mktime(start_time.timetuple())),
                'end_time': None,
                'x1': None,
                'description': description,
                'id': None,
                'category': category,
                'x4': None,
                'x5': None,
                'x6': None
            }

        return None

    def _call(self, call, *args, **kwargs):
        """Execute a request."""

        if self._throttle_last and (datetime.now() - self._throttle_last).total_seconds() < self._throttle_secs:
            print("Throttling %s for %d seconds" % (self.get_id(), self._throttle_secs))
            time.sleep(self._throttle_secs)
            self._throttle_count += 1
        else:
            self._throttle_count = 0

        if self._throttle_count > self._throttle_limit:
            raise RuntimeError("Throttled %d consecutive calls in %s, suspect a loop" % (self._throttle_count, self.get_id()))

        self._throttle_last = datetime.now()

        result = call(*args, **kwargs)

        return result
